<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Страница опроса</title>

    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
</head>
<body>
<div class="poll">

    <header class="header">
        <div class="header__wrap">
            <a href="#" class="header__logo" >
                <img src="img/logo.png" alt="" class="header__logo-img">
            </a>
            <p class="header__link">
                <a class="header__link-text" href="https://coopervision.ru/" >coopervision.ru</a>
            </p>
        </div>
    </header>

    <div class="main">
        <div class="main__wrap">
            <div class="main__top">
                <div class="main__top-svg svg-ballot"></div>
                <p class="main__top-title">Нам важно ваше мнение. Пройдите опрос и помогите нам стать лучше.</p>
            </div>
            <form class="main__form" action="/handler.php" method="POST">

                <div class="main__question" id="start">
                    <div class="main__question-number">1</div>
                    <p class="main__question-text">Как вы узнали о программе «Яркое Будущее™» с линзами MiSight® 1 day от CooperVision</p>
                </div>
                <div id="msg-erorr-5" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio">
                    <input type="radio" class="radio" name="q1" value="увидел(а) рекламу в Интернете" id="v1">
                    <label for="v1" class="radio__label"  >увидел(а) рекламу в Интернете</label>
                    <input type="radio" class="radio" name="q1" value="увидел(а) пост блогера" id="v2">
                    <label for="v2" class="radio__label"  >увидел(а) пост блогера
                    </label>
                    <input type="radio" class="radio" name="q1" value="увидела в группе в соц.сетях" id="v3">
                    <label for="v3" class="radio__label"  >увидела в группе в соц.сетях</label>
                    <input type="radio" class="radio" name="q1" value="порекомендовали друзья/знакомые" id="v4">
                    <label for="v4" class="radio__label"  >порекомендовали друзья/знакомые
                    </label>
                    <input type="radio" class="radio" name="q1" value="порекомендовал специалист в оптике" id="v5">
                    <label for="v5" class="radio__label"  >порекомендовал специалист в оптике
                    </label>
                    <input type="radio" class="radio" name="q1" value="прочитал(а) отзывы" id="v6">
                    <label for="v6" class="radio__label"  >прочитал(а) отзывы
                    </label>
                    <input type="radio" class="radio" name="q1" value="Другое" id="v7">
                    <label for="v7" class="radio__label"  >Другое ( введите свой вариант)
                    </label>
                </div>
                <textarea class="main__textarea jsTextArea" name="q1t" cols="30" rows="10" placeholder="Введите свой вариант" maxlength="300"></textarea>


                <div class="main__question" id="chooseOptics">
                    <div class="main__question-number">2</div>
                    <p class="main__question-text">В какой социальной сети вы видели информацию, по возможности укажите название группы</p>
                </div>
                <div id="msg-erorr-1" class="msg-erorr main__for-select-text hide">Выберите оптику</div>
                <textarea class="main__textarea jsTextArea" name="q2" cols="30" rows="10" placeholder="Введите свой вариант"  ="8" maxlength="300"></textarea>




                <!-- <div class="main__for-select">
                    <p class="main__for-select-text">Вы запрашивали данные по следующей оптике: <span id="ref-object"></span></p>
                    <input id="refobject-input" type="hidden" name="refobject">
                </div> -->
                <!--                <div class="main__for-select main__for-select-2">-->
                <!--                    &lt;!&ndash; <p class="main__for-select-text">Если вы посетили другую оптику, выберите ее из списка оптик:</p> &ndash;&gt;-->
                <!--                    <div class="main__for-select-select select main__for-select-select&#45;&#45;2"  ="4">-->
                <!--                        <input class="select__input" type="hidden" name="region" data-action="getcity"  >-->
                <!--                        <div class="select__head">Выберите регион из списка</div>-->
                <!--                        <ul id="region_list" class="select__list">-->
                <!--                        </ul>-->
                <!--                    </div>-->
                <!--                    <div class="main__for-select-select select main__for-select-select&#45;&#45;3"  ="5">-->
                <!--                        <input class="select__input" type="hidden" name="city" data-action="getobject">-->
                <!--                        <div class="select__head">Выберите город из списка</div>-->
                <!--                        <ul id="city_list" class="select__list">-->
                <!--                        </ul>-->
                <!--                    </div>-->
                <!--                    <div class="main__for-select-select select main__for-select-select&#45;&#45;4"  ="6">-->
                <!--                        <input class="select__input" type="hidden" name="bject" data-action="object">-->
                <!--                        <div class="select__head">Выберите оптику из списка</div>-->
                <!--                        <ul id="object_list" class="select__list">-->
                <!--                        </ul>-->
                <!--                    </div>-->
                <!--                </div>-->

                <div class="main__question main__question-3" id="quality">
                    <div class="main__question-number">3</div>
                    <p class="main__question-text">На каком сайте вы прочитали отзыв</p>
                </div>
                <div id="msg-erorr-3" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio main__radio-2">
                    <input type="radio" class="radio" name="q3" value="irecommend.ru" id="q1">
                    <label for="q1" class="radio__label radio__label--q"  >irecommend.ru</label>
                    <input type="radio" class="radio" name="q3" value="Отзовик" id="q2">
                    <label for="q2" class="radio__label radio__label--q" >Отзовик
                    </label>
                    <input type="radio" class="radio jsRadioTextArea" name="q3" value="Другое" id="q3">
                    <label for="q3" class="radio__label radio__label--q"  >Другое (введите свой вариант)</label>
                </div>
                <textarea class="main__textarea jsTextArea" name="q3" cols="30" rows="10" placeholder="Введите свой вариант"  maxlength="300"></textarea>




                <div class="main__question" id="">
                    <div class="main__question-number">4</div>
                    <p class="main__question-text">Есть ли у ваших детей близорукость</p>
                </div>
                <div id="" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio">
                    <input type="radio" class="radio" name="q4" value="Да" id="v8">
                    <label for="v8" class="radio__label">Да</label>
                    <input type="radio" class="radio" name="q4" value="Нет" id="v9">
                    <label for="v9" class="radio__label" >Нет</label>
                </div>




                <div class="main__question" id="">
                    <div class="main__question-number">5</div>
                    <p class="main__question-text">Как часто вы проверяете зрение ребенка?</p>
                </div>
                <div id="" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio">
                    <input type="radio" class="radio" name="q5" value="раз в 6 месяцев" id="v10">
                    <label for="v10" class="radio__label">раз в 6 месяцев</label>
                    <input type="radio" class="radio" name="q5" value="раз в год" id="v11">
                    <label for="v11" class="radio__label" >раз в год</label>
                    <input type="radio" class="radio" name="q5" value="реже, чем раз в год" id="v12">
                    <label for="v12" class="radio__label" >реже, чем раз в год</label>
                </div>



                <div class="main__question" id="">
                    <div class="main__question-number">6</div>
                    <p class="main__question-text">Ребенок носит очки или линзы?</p>
                </div>
                <div id="" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio">
                    <input type="radio" class="radio" name="q6" value="очки" id="v13">
                    <label for="v13" class="radio__label">очки</label>
                    <input type="radio" class="radio" name="q6" value="дневные линзы" id="v14">
                    <label for="v14" class="radio__label" >дневные линзы</label>
                    <input type="radio" class="radio" name="q6" value="ночные линзы" id="v15">
                    <label for="v15" class="radio__label" >ночные линзы</label>
                </div>



                <div class="main__question" id="">
                    <div class="main__question-number">7</div>
                    <p class="main__question-text">Посетили ли вы оптику после изучения материалов о программе «Яркое Будущее™»
                    </p>
                </div>
                <div id="" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio">
                    <input type="radio" class="radio" name="q7" value="Да" id="v16">
                    <label for="v16" class="radio__label">Да</label>
                    <input type="radio" class="radio" name="q7" value="Нет" id="v17">
                    <label for="v17" class="radio__label" >Нет</label>
                </div>





                <div class="main__question main__question-3" id="no-visit">
                    <div class="main__question-number">8</div>
                    <p class="main__question-text">Если вы не посетили оптику, укажите причину:</p>
                </div>
                <div id="msg-erorr-4" class="msg-erorr main__for-select-text hide">Выберите ответ</div>
                <div class="main__radio main__radio-2">
                    <input type="radio" class="radio" name="q7" value="Неудобное расположение оптики" id="n-v1">
                    <label for="n-v1" class="radio__label radio__label--n-v"  >Неудобное расположение оптики</label>
                    <input type="radio" class="radio" name="q7" value="Обратился(ась) в другую клинику/поликлинику" id="n-v2">
                    <label for="n-v2" class="radio__label radio__label--n-v"  >Обратился(ась) в другую клинику/поликлинику</label>
                    <input type="radio" class="radio" name="q7" value="Обратился(ась) в другую клинику/поликлинику" id="n-v3">
                    <label for="n-v3" class="radio__label radio__label--n-v"  >Не смог(ла) записаться к специалисту</label>
                    <input type="radio" class="radio" name="q7" value="Не смог(ла) дозвониться до оптики" id="n-v4">
                    <label for="n-v4" class="radio__label radio__label--n-v"  >Не смог(ла) дозвониться до оптики</label>
                    <input type="radio" class="radio" name="q7" value="Высокая стоимость консультации в оптике" id="n-v5">
                    <label for="n-v5" class="radio__label radio__label--n-v"  >Высокая стоимость консультации в оптике</label>
                    <input type="radio" class="radio" name="q7" value="Ребенок носит очки, все устраивает" id="n-v6">
                    <label for="n-v6" class="radio__label radio__label--n-v"  >Ребенок носит очки, все устраивает</label>
                    <input type="radio" class="radio" name="q7" value="Ребенок носит ночные линзы, все устраивает" id="n-v7">
                    <label for="n-v7" class="radio__label radio__label--n-v"  >Ребенок носит ночные линзы, все устраивает</label>
                    <input type="radio" class="radio" name="q7" value="Обратился(ась) к специалисту, который ранее наблюдал ребенка" id="n-v8">
                    <label for="n-v8" class="radio__label radio__label--n-v" >Обратился(ась) к специалисту, который ранее наблюдал ребенка</label>
                    <input type="radio" class="radio jsRadioTextArea2" name="q7" value="Другое" id="n-v9">
                    <label for="n-v9" class="radio__label radio__label--n-v"  >Другое (введите свой вариант)</label>
                </div>
                <textarea class="main__textarea jsTextArea2" name="q7t" cols="30" rows="10" placeholder="Введите свой вариант"   maxlength="300"></textarea>



                <div class="main__question main__question-2" id="grade">
                    <div class="main__question-number">9</div>
                    <p class="main__question-text">Оцените качество работы оптики по программе замедления близорукости «Яркое Будущее™» с линзами MiSight® 1 day по шкале от 1 до 5:</p>
                </div>
                <div id="msg-erorr-2" class="msg-erorr main__for-select-text hide">Поставьте оценку</div>
                <div class="main__radio main__radio-1">
                    <input type="radio" class="radio" name="q9" value="1" id="gr1">
                    <label for="gr1" class="radio__label radio__label--gr"  >1</label>
                    <input type="radio" class="radio" name="q9" value="2" id="gr2">
                    <label for="gr2" class="radio__label radio__label--gr"  >2</label>
                    <input type="radio" class="radio" name="q9" value="3" id="gr3">
                    <label for="gr3" class="radio__label radio__label--gr"  >3</label>
                    <input type="radio" class="radio" name="q9" value="4" id="gr4">
                    <label for="gr4" class="radio__label radio__label--gr"  >4</label>
                    <input type="radio" class="radio" name="q9" value="5" id="gr5">
                    <label for="gr5" class="radio__label radio__label--gr"  >5</label>
                </div>


                <div class="main__question" id="">
                    <div class="main__question-number">10</div>
                    <p class="main__question-text">До 31 октября приглашаем вас  принять участие в акции от CooperVision и получить бесплатную консультацию для вашего ребенка и пару лечебных линз MiSight® 1 day. Условия акции по
                        <a href="https://coopervision.ru/misight/find-specialist-nearby?utm_medium=email&utm_campaign=oprosnik&utm_term=october">ссылке</a>
                    </p>
                </div>




                <button class="main__button" type="submit" >Отправить</button>
            </form>
        </div>
    </div>

    <div class="ruoverlay">
        <div class="ruoverlay__wrap">
            <p class="ruoverlay__text">Имеются противопоказания. Необходимо проконсультироваться со специалистом</p>
        </div>
    </div>

    <footer class="footer">
        <div class="footer__wrap">
            <p class="footer__text">Москва, «Башня «Империя», Пресненская наб., 6, строение 2, помещение 1707/1
                <a class="footer__tel" href="tel:+78002007744"  >+7 (800) 200-77-44</a>
                <a class="footer__tel" href="tel:+74959958015"  >+7 495 995 80 15</a>
            </p>
            <p class="footer__text">&copy; 2020 CooperVision | Подразделение компании <a href="https://www.coopercos.com/" class="footer__link" target="_blank"  >CooperCompanies</a></p>
        </div>
    </footer>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>