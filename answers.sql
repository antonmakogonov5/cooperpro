-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 26 2021 г., 19:05
-- Версия сервера: 8.0.19
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cooper`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int UNSIGNED NOT NULL,
  `q1` varchar(100) DEFAULT NULL,
  `q1t` text,
  `q2` text,
  `q3` varchar(100) DEFAULT NULL,
  `q3t` text,
  `q4` varchar(100) DEFAULT NULL,
  `q5` varchar(100) DEFAULT NULL,
  `q6` varchar(100) DEFAULT NULL,
  `q7` varchar(100) DEFAULT NULL,
  `q8` varchar(100) DEFAULT NULL,
  `q8t` varchar(100) DEFAULT NULL,
  `q9` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `q1`, `q1t`, `q2`, `q3`, `q3t`, `q4`, `q5`, `q6`, `q7`, `q8`, `q8t`, `q9`, `created_at`) VALUES
(5, NULL, '', 'sdfsdfs', '', NULL, 'Да', NULL, NULL, NULL, NULL, NULL, '5', '2021-10-26 15:38:47'),
(6, NULL, '', 'sdfsdfs', '', NULL, 'Да', NULL, NULL, NULL, NULL, NULL, '5', '2021-10-26 16:00:42'),
(7, NULL, '', 'sdfsdfs', '', NULL, 'Да', NULL, NULL, NULL, NULL, NULL, '4', '2021-10-26 16:01:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
