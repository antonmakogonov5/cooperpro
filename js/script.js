$(document).ready(function() {

    let analitic = false,
        metric = false;

    function test1() {
        if (typeof ga === 'undefined') {
            console.log('%c CooperVision: Аналитика не подключена!', 'color: red');
        } else {
            console.log('%c CooperVision: Аналитика успешно подключена!', 'color: green');
            analitic = true;
        }
        if (typeof ym === 'undefined') {
            console.log('%c CooperVision: Метрика не подключена!', 'color: red');
        } else {
            console.log('%c CooperVision: Метрика успешно подключена!', 'color: green');
            metric = true;
        }
    }
    function googleSend(event, type, category, name) {
        if (analitic === true) {
            ga(event, type, category, name);
        } else {
            console.log('%c CooperVision: Аналитика не подключена!', 'color: red');
        }
    }
    function yandexSend(number, event, name) {
        if (metric === true) {
            ym(number, event, name);
        } else {
            console.log('%c CooperVision: Метрика не подключена!', 'color: red');
        }
    }
    test1();





    /* Открытие селекта */
    $('.select').on('click', function () {
        $(this).toggleClass('open');
    });
    /* Открытие селекта табом */
    $('.select').keypress(function(e) {
        if (e.which == 13) {
            $(this).toggleClass('open');
        }
    });

    /* Смена текста и значения по событию на итем селекта + закрытие селекта табом */
    $('.select__list').on('click keypress', '.select__item', function (e) {
        e.stopPropagation();

        let itemText = $(this).text();
        let itemValue = $(this).data('value');
        let select = $(this).closest('.select');

        select.find('.select__head').text(itemText);
        select.find('.select__input').val(itemValue).trigger("change");
        select.addClass('active').removeClass('open');
    });

    /* Закрытие всех селектов при клике вне блока селектов */
    $(document).on('click', function (e) {
        if ( !$('.select').is(e.target) && !$('.select').is(e.target) && $('.select').has(e.target).length === 0) {
            $('.select').removeClass('open');
        };
    });

    /* выбор радиокнопки табом */
    $('.radio__label').keypress(function(e) {
        if (e.which == 13) {
            const radioNum = $(this).attr('for');
            const radioName = $(`#${radioNum}`).attr('name');
            $(`input[name="${radioName}"]`).prop('checked', false);
            $(`#${radioNum}`).prop('checked', true);

            if ( radioName === 'quality' ) {
                /*  Появление TextArea При табе на jsTextArea */
                if ( $(`#${radioNum}`).hasClass('jsRadioTextArea') ) {
                    $('.jsTextArea').removeClass('hide');
                } else {
                    $('.jsTextArea').addClass('hide');
                }
            } else if ( radioName === 'no-visit' ) {
                /*  Появление TextArea При табе на jsTextArea */
                if ( $(`#${radioNum}`).hasClass('jsRadioTextArea2') ) {
                    $('.jsTextArea2').removeClass('hide');
                } else {
                    $('.jsTextArea2').addClass('hide');
                }
            }
        }
    });

    /* Появление TextArea При клике на jsTextArea */
    $('input[name="quality"]').on('change', function() {
        console.log($(this));
        if(this.checked) {
            console.log($(this).hasClass('jsRadioTextArea'))
            if ( $(this).hasClass('jsRadioTextArea') ) {
                $('.jsTextArea').removeClass('hide');
            } else {
                $('.jsTextArea').addClass('hide');
            }
        }
    });

    $('input[name="no-visit"]').on('change', function() {
        console.log($(this));
        if(this.checked) {
            console.log($(this).hasClass('jsRadioTextArea2'));
            if ( $(this).hasClass('jsRadioTextArea2') ) {
                $('.jsTextArea2').removeClass('hide');
            } else {
                $('.jsTextArea2').addClass('hide');
            }
        }
    });

    /* плавный скролл*/
    function scrollGo(id) {
        let block = $(`#${id}`).offset().top;
        $("html, body").animate({ scrollTop: block}, 1000);
    }

    /* Действия при сабмите */
    // $(document).on('submit','.main__form',function(e) {
    //     e.preventDefault();
    //
    //     googleSend('send', 'event', 'pro_submit', 'pick_submit');
    //     yandexSend(70071583, 'reachGoal', 'pick_submit');
    //
    //     $('.msg-erorr').addClass('hide');
    //     var up = false;
    //
    //     let fields = $(this).serialize();
    //     $.ajax({
    //         type: "POST",
    //         url: '/ajax/ajax.php',
    //         data: {action : 'submitform', formdata : fields},
    //         success: function(responce){
    //             let data = $.parseJSON(responce);
    //             if(data.error.length > 0) {
    //                 // alert('AAA');
    //                 data.error.forEach(
    //                     function findError( item ) {
    //                         if (item === 'visit') {
    //                             $('#msg-erorr-5').removeClass('hide');
    //                             if (up == false) {
    //                                 scrollGo('start');
    //                                 up = true;
    //                             }
    //                         }
    //                         if (item === 'bject') {
    //                             $('#msg-erorr-1').removeClass('hide');
    //                             if (up == false) {
    //                                 scrollGo('msg-erorr-1');
    //                                 up = true;
    //                             }
    //                         }
    //                         if (item === 'grade') {
    //                             $('#msg-erorr-2').removeClass('hide');
    //                             if (up == false) {
    //                                 scrollGo('msg-erorr-2');
    //                                 up = true;
    //                             }
    //                         }
    //                         if (item === 'quality') {
    //                             $('#msg-erorr-3').removeClass('hide');
    //                             if (up == false) {
    //                                 scrollGo('msg-erorr-3');
    //                                 up = true;
    //                             }
    //                         }
    //                         if (item === 'no-visit') {
    //                             $('#msg-erorr-4').removeClass('hide');
    //                             if (up == false) {
    //                                 scrollGo('msg-erorr-4');
    //                                 up = true;
    //                             }
    //                         }
    //                     });
    //             } else {
    //                 /* Модалка */
    //                 $.magnificPopup.open({
    //                     items: {
    //                         src: '#thanks'
    //                     },
    //                     callbacks: {
    //                         open: function () {
    //                             if (document.body.clientWidth > 700) {
    //                                 $(this.container).find('.mfp-content').css({
    //                                     'width': '700px'
    //                                 });
    //                             } else {
    //                                 $(this.container).find('.mfp-content').css({
    //                                     'width': '300px'
    //                                 });
    //                             }
    //                             $(this.container).find('.mfp-close').addClass('new-close');
    //                         },
    //                         close: function () {
    //                             window.location.href = 'http://coopervision.ru/misight?utm_source=oprosnik_locator&utm_medium=after_interview/';
    //                         }
    //                     }
    //                 });
    //             }
    //
    //         }
    //     });
    // });

    let params =  (new URL(document.location)).searchParams;
    let id = params.get("optics");
    if (id !== null) {
        // $.ajax({
        //     type: "POST",
        //     url: '/ajax/ajax.php',
        //     data: {action : 'getrefobject', id : id},
        //     success: function(responce){
        //         let data = $.parseJSON(responce);
        //         $('#ref-object').text(data.obj);
        //         $('#refobject-input').val(data.value);
        //     }
        // });
    } else {
        $('#ref-object').text("данные отсутствуют");
        $('#refobject-input').val('');
    }



    getListEntity('getregion');

    $(document).on('change','.select__input',function(e) {
        let action = $(e.target).data('action');
        let value = $(e.target).val();
        getListEntity(action, value);
        if (action == 'getcity') {

            
            googleSend('send', 'event', 'pro_optics', 'pick_region');
            yandexSend(70071583, 'reachGoal', 'pick_region');
            // ga('send', 'event', 'pro_optics', 'pick_region');
            // gtag('event', 'pick_region', {
            //     'event_category': 'pro_optics'
            //   });
            // ym(70071583, 'reachGoal', 'pick_region');
            console.log('выбор региона');

        } else if (action == 'getobject') {

            googleSend('send', 'event', 'pro_optics', 'pick_city');
            yandexSend(70071583, 'reachGoal', 'pick_city');
            // ga('send', 'event', 'pro_optics', 'pick_city');
            // ym(70071583, 'reachGoal', 'pick_city');
            console.log('выбор города');

        } else if (action == 'object') {

            googleSend('send', 'event', 'pro_optics', 'pick_optics');
            yandexSend(70071583, 'reachGoal', 'pick_optics');
            // ga('send', 'event', 'pro_optics', 'pick_optics');
            // ym(70071583, 'reachGoal', 'pick_optics');
            console.log('выбор оптики');

        }
    });

    /* действия на первом ответе*/
    $('.radio[name="visit"]').on('change', function(e) {
        let val = +$(this).val();
        switch (val) {
            case 1:
                scrollGo('chooseOptics');

                $('.radio[name="no-visit"]').attr('disabled','disabled');
                $('.radio__label--n-v').addClass('disabled-radio');
                $('.jsTextArea2').attr('disabled','disabled').addClass('disabled-textarea');
                $('#no-visit .main__question-number').addClass('disabled-question');
                $('#no-visit .main__question-text').addClass('disabled-text');

                $('#chooseOptics .main__question-number').removeClass('disabled-question');
                $('#chooseOptics .main__question-text').removeClass('disabled-text');
                $('.main__for-select-select').removeClass('disabled-select');
                $('.select__head').removeClass('disabled-head');
                $('#grade .main__question-number').removeClass('disabled-question');
                $('#grade .main__question-text').removeClass('disabled-text');
                $('.radio__label--gr').removeClass('disabled-radio');
                $('.radio[name="grade"]').removeAttr('disabled');
                $('#quality .main__question-number').removeClass('disabled-question');
                $('#quality .main__question-text').removeClass('disabled-text');
                $('.radio__label--q').removeClass('disabled-radio');
                $('.jsTextArea').removeAttr('disabled').removeClass('disabled-textarea');
                $('.radio[name="quality"]').removeAttr('disabled');

                googleSend('send', 'event', 'pro_visit', 'pick_yes');
                yandexSend(70071583, 'reachGoal', 'pick_yes');
                console.log('Ответ Да');
            break;
            case 2:
                scrollGo('no-visit');

                $('.radio[name="no-visit"]').removeAttr('disabled');
                $('.radio__label--n-v').removeClass('disabled-radio');
                $('.jsTextArea2').removeAttr('disabled').removeClass('disabled-textarea');
                $('#no-visit .main__question-number').removeClass('disabled-question');
                $('#no-visit .main__question-text').removeClass('disabled-text');
                
                $('#chooseOptics .main__question-number').addClass('disabled-question');
                $('#chooseOptics .main__question-text').addClass('disabled-text');
                $('.main__for-select-select').addClass('disabled-select');
                $('.select__head').addClass('disabled-head');
                $('#grade .main__question-number').addClass('disabled-question');
                $('#grade .main__question-text').addClass('disabled-text');
                $('.radio__label--gr').addClass('disabled-radio');
                $('.radio[name="grade"]').attr('disabled','disabled');
                $('#quality .main__question-number').addClass('disabled-question');
                $('#quality .main__question-text').addClass('disabled-text');
                $('.radio__label--q').addClass('disabled-radio');
                $('.jsTextArea').attr('disabled','disabled').addClass('disabled-textarea');
                $('.radio[name="quality"]').attr('disabled','disabled');

                googleSend('send', 'event', 'pro_visit', 'pick_no');
                yandexSend(70071583, 'reachGoal', 'pick_no');
                console.log('Ответ Нет');
            break;
        }
    });

    $('.radio[name="grade"]').on('change', function(e) {
        let val = +$(this).val();
        switch (val) {
            case 5:
                googleSend('send', 'event', 'pro_rating', 'pick_rating-5');
                yandexSend(70071583, 'reachGoal', 'pick_rating-5');
                // ga('send', 'event', 'pro_rating', 'pick_rating-5');
                // ym(70071583, 'reachGoal', 'pick_rating-5');
                console.log('Оценка 5');
            break;
            case 4:
                googleSend('send', 'event', 'pro_rating', 'pick_rating-4');
                yandexSend(70071583, 'reachGoal', 'pick_rating-4');
                // ga('send', 'event', 'pro_rating', 'pick_rating-4');
                // ym(70071583, 'reachGoal', 'pick_rating-4');
                console.log('Оценка 4');
            break;
            case 3:
                googleSend('send', 'event', 'pro_rating', 'pick_rating-3');
                yandexSend(70071583, 'reachGoal', 'pick_rating-3');
                // ga('send', 'event', 'pro_rating', 'pick_rating-3');
                // ym(70071583, 'reachGoal', 'pick_rating-3');
                console.log('Оценка 3');
            break;
            case 2:
                googleSend('send', 'event', 'pro_rating', 'pick_rating-2');
                yandexSend(70071583, 'reachGoal', 'pick_rating-2');
                // ga('send', 'event', 'pro_rating', 'pick_rating-2');
                // ym(70071583, 'reachGoal', 'pick_rating-2');
                console.log('Оценка 2');
            break;
            case 1:
                googleSend('send', 'event', 'pro_rating', 'pick_rating-1');
                yandexSend(70071583, 'reachGoal', 'pick_rating-1');
                // ga('send', 'event', 'pro_rating', 'pick_rating-1');
                // ym(70071583, 'reachGoal', 'pick_rating-1');
                console.log('Оценка 1');
            break;
        }
    });

    $('.radio[name="quality"]').on('change', function(e) {
        let val = +$(this).val();
        switch (val) {
            case 1:
                googleSend('send', 'event', 'pro_better', 'pick_communication');
                yandexSend(70071583, 'reachGoal', 'pick_communication');
                // ga('send', 'event', 'pro_better', 'pick_communication');
                // ym(70071583, 'reachGoal', 'pick_communication');
                console.log('Качество коммуникации');
            break;
            case 2:
                googleSend('send', 'event', 'pro_better', 'pick_service-speed');
                yandexSend(70071583, 'reachGoal', 'pick_service-speed');
                // ga('send', 'event', 'pro_better', 'pick_service-speed');
                // ym(70071583, 'reachGoal', 'pick_service-speed');
                console.log('Скорость обслуживания');
            break;
            case 3:
                googleSend('send', 'event', 'pro_better', 'pick_other');
                yandexSend(70071583, 'reachGoal', 'pick_other');
                // ga('send', 'event', 'pro_better', 'pick_other');
                // ym(70071583, 'reachGoal', 'pick_other');
                console.log('Другое (введите свой вариант)');
            break;
        }
    });

    $('.radio[name="no-visit"]').on('change', function(e) {
        let val = +$(this).val();
        switch (val) {
            case 1:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_bad_location');
                yandexSend(70071583, 'reachGoal', 'pick_bad_location');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_communication');
                // ym(70071583, 'reachGoal', 'pick_communication');
                console.log('Неудобное расположение оптики');
            break;
            case 3:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_no_sign_up_with_specialist');
                yandexSend(70071583, 'reachGoal', 'pick_no_sign_up_with_specialist');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_service-speed');
                // ym(70071583, 'reachGoal', 'pick_service-speed');
                console.log('Не смог(ла) записаться к специалисту');
            break;
            case 4:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_no_call');
                yandexSend(70071583, 'reachGoal', 'pick_no_call');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_other');
                // ym(70071583, 'reachGoal', 'pick_other');
                console.log('Не смог(ла) дозвониться до оптики');
            break;
            case 5:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_high_cost');
                yandexSend(70071583, 'reachGoal', 'pick_high_cost');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_communication');
                // ym(70071583, 'reachGoal', 'pick_communication');
                console.log('Высокая стоимость консультации в оптике');
            break;
            case 6:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_child_wears_glasses');
                yandexSend(70071583, 'reachGoal', 'pick_child_wears_glasses');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_service-speed');
                // ym(70071583, 'reachGoal', 'pick_service-speed');
                console.log('Ребенок носит очки, все устраивает');
            break;
            case 7:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_child_wears_night_linses');
                yandexSend(70071583, 'reachGoal', 'pick_child_wears_night_linses');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_other');
                // ym(70071583, 'reachGoal', 'pick_other');
                console.log('Ребенок носит ночные линзы, все устраивает');
            break;
            case 8:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_go_specialist');
                yandexSend(70071583, 'reachGoal', 'pick_go_specialist');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_other');
                // ym(70071583, 'reachGoal', 'pick_other');
                console.log('Обратился(ась) к специалисту, который ранее наблюдал ребенка');
            break;
            case 2:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_another_clinic');
                yandexSend(70071583, 'reachGoal', 'pick_another_clinic');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_other');
                // ym(70071583, 'reachGoal', 'pick_other');
                console.log('Обратился(ась) в другую клинику/поликлинику');
            break;
            case 9:
                googleSend('send', 'event', 'pro-why-no-visit', 'pick_no-visit_other');
                yandexSend(70071583, 'reachGoal', 'pick_no-visit_other');
                // ga('send', 'event', 'pro-why-no-visit', 'pick_other');
                // ym(70071583, 'reachGoal', 'pick_other');
                console.log('Другое (введите свой вариант)');
            break;
        }
    });

    /* клик по текстареа*/
    $('.jsTextArea').on('click', function() {
        if (!$('.jsRadioTextArea').checked) {
            $('.jsRadioTextArea').prop("checked", true);
        }
    });

    $('.jsTextArea2').on('click', function() {
        if (!$('.jsRadioTextArea2').checked) {
            $('.jsRadioTextArea2').prop("checked", true);
        }
    });
});

function getListEntity(action, id = '') {
    let sel;
    let selectobject;
    if(action == 'getregion') {
        sel = $('#region_list');
    } else if (action == 'getcity') {

        sel = $('#object_list');
        sel.empty();
        selectobject = sel.closest('.select');
        selectobject.find('.select__head').text('Выберите город из списка');
        selectobject.find('.select__input').val('');

        sel = $('#city_list');
        sel.empty();
        selectobject = sel.closest('.select');
        selectobject.find('.select__head').text('Выберите город из списка');
        selectobject.find('.select__input').val('');

    } else if (action == 'getobject') {
        sel = $('#object_list');
        sel.empty();
        selectobject = sel.closest('.select');
        selectobject.find('.select__head').text('Выберите город из списка');
        selectobject.find('.select__input').val('');
    } else {
        return false;
    }

    // $.ajax({
    //     type: "POST",
    //     url: '/ajax/ajax.php',
    //     data: {action : action, id : id},
    //     success: function(responce){
    //         let list = $.parseJSON(responce);
    //         sel.find('li').unbind();
    //         sel.empty();
    //         list.forEach(function(element, idex){
    //             sel.append('<li class="select__item" tabindex="'+idex+'" data-value="'+element.value+'">'+element.name+'</li>');
    //         });
    //     }
    // });
};