<?php

try {
    $host = '127.0.0.1';
    $db   = 'cooper';
    $user = 'mysql';
    $pass = 'mysql';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $pdo = new PDO($dsn, $user, $pass, $opt);


    function pdoSet($allowed, &$values, $source = array()) {
        $set = '';
        $values = array();
        if (!$source) $source = &$_POST;
        foreach ($allowed as $field) {
            if (isset($source[$field])) {
                $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
                $values[$field] = htmlspecialchars(trim(($source[$field])));
            }
        }
        return substr($set, 0, -2);
    }

    $allowed = array('q1','q1t', 'q2','q3','q3t' ,'q4', 'q5','q6', 'q7','q8' , 'q8t', 'q9'); // allowed fields
    $sql = "INSERT INTO answers SET ".pdoSet($allowed,$values);
    $stm = $pdo->prepare($sql);
    $stm->execute($values);

} catch (\Exception $exception) {
    $log_file_name = $_SERVER['DOCUMENT_ROOT']."/my_log.txt";
    $now = date("Y-m-d H:i:s");
    $string = json_encode($exception);
    file_put_contents($log_file_name, $now." ".$string."\r\n", FILE_APPEND);
}


